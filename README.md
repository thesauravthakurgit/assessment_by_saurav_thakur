For Django :

### pip3

### python3

create virtualenv

### env

activate the virtualenv

Install all the requirments in the requirment.txt By:

### pip3 install -r requirments.txt

Make migration By:

### python3 manage.py makemigrations

Migrate By:

### python3 manage.py migrate

Run the django project on port 8000:

### 127.0.0.1:8000

By :

### python3 manage.py runserver
