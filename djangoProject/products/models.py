from django.db import models


# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    product_image = models.ImageField(upload_to='product_images/',
                                      blank=True,
                                      null=True)
    product_document = models.FileField(upload_to='product_documents/',
                                        blank=True,
                                        null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
