from rest_framework import serializers
from .models import Product
from rest_framework.utils import model_meta
import os


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def removeFileImage(self, path):
        print('saurav', path == '')
        if path != '':
            BASE_DIR = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)))
            BASE_DIR = os.path.join(BASE_DIR, 'media/')
            data_path = os.path.join(BASE_DIR, f'{path}')
            if os.path.exists(data_path):
                print(data_path)
                os.remove(data_path)

    def update(self, instance, validated_data):
        # raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)

        m2m_fields = []
        for attr, value in validated_data.items():
            # print(attr, value)
            # print(attr, value, instance.product_document)
            if attr == 'product_image' and value is None:
                print(instance.product_image, 'saurav')
                self.removeFileImage(instance.product_image)
            elif attr == 'product_image':
                self.removeFileImage(instance.product_image)
            if attr == 'product_document' and value is None:
                print(instance.product_document)
                self.removeFileImage(instance.product_document)
            elif attr == 'product_document':
                self.removeFileImage(instance.product_document)
            if attr in info.relations and info.relations[attr].to_many:
                m2m_fields.append((attr, value))
            else:
                setattr(instance, attr, value)

        instance.save()

        for attr, value in m2m_fields:
            field = getattr(instance, attr)
            field.set(value)

        return instance
