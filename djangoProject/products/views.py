from django.shortcuts import render
from rest_framework import viewsets
from .serializer import ProductSerializer
from .models import Product
from rest_framework.response import Response
import os
from django.db import transaction
from rest_framework import status

# Create your views here.

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class ProductViews(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    def removeFileImage(self, path):
        if path != '':
            BASE_DIR = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)))
            BASE_DIR = os.path.join(BASE_DIR, 'media/')
            data_path = os.path.join(BASE_DIR, f'{path}')
            if os.path.exists(data_path):
                print(data_path)
                os.remove(data_path)

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if (instance.product_image != ''):
            self.removeFileImage(instance.product_image)
        if (instance.product_document != ''):
            self.removeFileImage(instance.product_document)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
